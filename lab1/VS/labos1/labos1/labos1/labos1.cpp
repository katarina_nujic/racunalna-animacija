﻿#define _USE_MATH_DEFINES
#include <GL/glut.h>
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

using namespace std;

class Point {
public:
    float x;
    float y;
    float z;
    float h;

    Point() {
        this->x = 0;
        this->y = 0;
        this->z = 0;
        this->h = 1.0;
    }

    Point(float x, float y, float z, float h) {
        this->x = x;
        this->y = y;
        this->z = z;
        this->h = h;
    }

    Point(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
        this->h = 1.0;
    }

    friend ostream& operator<<(ostream& os, const Point& p) {
        os << p.x << " " << p.y << " " << p.z << " " << endl;
        return os;
    }

    void normalize_then_scale(int scale) {
        float sum = sqrt(pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2));
        this->x = (this->x / sum) * scale;
        this->y = (this->y / sum) * scale;
        this->z = (this->z / sum) * scale;
    }

    void normalize_then_scale() {
        int scale = 1;
        float sum = sqrt(pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2));
        this->x = (this->x / sum) * scale;
        this->y = (this->y / sum) * scale;
        this->z = (this->z / sum) * scale;
    }

    float& operator[](int index) {
        switch (index) {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            cout << "Error, index out of range" << std::endl;
        }
    }
};

// Global variables
int wnd_width = 1400; // Set your desired window width
int wnd_height = 700; // Set your desired window height
string wnd_name = "lab1"; // Set your desired window name
vector<Point> spline_points;
vector<Point> calculated_spline_points;
vector<Point> spline_rot;
vector<Point> point_tangents;
vector<Point> rot_axis;
vector<float> angle;
vector<Point> verts;
vector<vector<int>> faces;
Point trajectory;
Point eye;
Point view;
Point source;
float xmin, xmax, ymin, ymax, zmin, zmax;
int step;
int max_step;


class Bspline {
public:
    static void read_spline_points(string file) {
        string lines;
        fstream MyReadFile(file);
        vector<float> values;
        Point vertex = Point();
        while (getline(MyReadFile, lines)) {
            stringstream ss(lines);
            string word;
            char del = ' ';
            values.clear();
            while (!ss.eof()) {
                getline(ss, word, del);
                values.push_back(stof(word));
            }

            vertex = Point(values[0], values[1], values[2]);
            spline_points.push_back(vertex);
        }
    }

    static void find_spline_points() {
        float b_3[4][4] = {
        {-1, 3, -3, 1},
        {3, -6, 3, 0},
        {-3, 0, 3, 0},
        {1, 4, 1, 0}
        };

        int steps = spline_points.size() - 3;
        std::vector<Point> points;
        for (int st = 0; st < steps; ++st) {
            float t = 0;
            float incr = 0.01;
            points.clear();
            copy(spline_points.begin(), spline_points.end(), back_inserter(points));
            float r_mat[4][4] = {
                {points[st].x, points[st].y, points[st].z, 1},
                {points[st + 1].x, points[st + 1].y, points[st + 1].z, 1},
                {points[st + 2].x, points[st + 2].y, points[st + 2].z, 1},
                {points[st + 3].x, points[st + 3].y, points[st + 3].z, 1}
            };
            /*cout << "r_mat: " << endl;
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    cout << r_mat[i][j];
                }
                cout << endl;
            }*/

            for (int i = 0; i < 100; ++i) {
                float t_mat[4] = { t * t * t / 6.0f, t * t / 6.0f, t / 6.0f, 1 / 6.0f };
                float p_t_temp[3] = { 0.0, 0.0, 0.0 };
                //cout << "t: " << t << endl;
                
                for (int row = 0; row < 4; ++row) {
                    for (int col = 0; col < 4; ++col) {
                        /*cout << "aaa " << endl;
                        cout << t_mat[row] << endl;
                        cout << b_3[row][col] << endl;
                        cout << r_mat[col][0] << endl;*/
                        p_t_temp[0] += t_mat[row] * b_3[row][col] * r_mat[col][0];
                        p_t_temp[1] += t_mat[row] * b_3[row][col] * r_mat[col][1];
                        p_t_temp[2] += t_mat[row] * b_3[row][col] * r_mat[col][2];
                    }
                }
                //cout << "p_t_temp " << p_t_temp[0] << " " << p_t_temp[1] << " " << p_t_temp[2] << endl;
                Point p_t(p_t_temp[0], p_t_temp[1], p_t_temp[2]);
                calculated_spline_points.push_back(p_t);
                t += incr;
            }
        }
    }

    static void find_spline_tangent() {
        float b_3d[3][4] = {
            {-1, 3, -3, 1},
            {2, -4, 2, 0},
            {-1, 0, 1, 0}
        };

        int steps = spline_points.size() - 3;

        for (int st = 0; st < steps; ++st) {
            float t = 0;
            float incr = 0.01;
            std::vector<Point> points;
            copy(spline_points.begin(), spline_points.end(), back_inserter(points));
            float r_mat[4][4] = {
                {points[st].x, points[st].y, points[st].z, 1},
                {points[st + 1].x, points[st + 1].y, points[st + 1].z, 1},
                {points[st + 2].x, points[st + 2].y, points[st + 2].z, 1},
                {points[st + 3].x, points[st + 3].y, points[st + 3].z, 1}
            };

            for (int i = 0; i < 100; ++i) {
                float t_mat[3] = { t * t /2.0f, t /2.0f, 1 /2.0f};
                float p_t_temp[3] = { 0.0f, 0.0f, 0.0f };

                for (int row = 0; row < 3; ++row) {
                    for (int col = 0; col < 4; ++col) {
                        p_t_temp[0] += t_mat[row] * b_3d[row][col] * r_mat[col][0];
                        p_t_temp[1] += t_mat[row] * b_3d[row][col] * r_mat[col][1];
                        p_t_temp[2] += t_mat[row] * b_3d[row][col] * r_mat[col][2];
                    }
                }

                Point p_t(p_t_temp[0], p_t_temp[1], p_t_temp[2]);
                point_tangents.push_back(p_t);
                t += incr;
            }
        }
    }


    static void calculate_model_angle() {
        float s[3] = { 0.0f, 0.0f, 1.0f };

        for (int i = 0; i < point_tangents.size(); ++i) {
            Point p = point_tangents[i];
            float e[3] = { p.x, p.y, p.z };

            float axis[3] = {
                s[1] * e[2] - s[2] * e[1],
                s[2] * e[0] - s[0] * e[2],
                s[0] * e[1] - s[1] * e[0]
            };
            rot_axis.push_back(Point(axis[0], axis[1], axis[2]));

            float dot_product = s[0] * e[0] + s[1] * e[1] + s[2] * e[2];
            float s_norm = sqrt(s[0] * s[0] + s[1] * s[1] + s[2] * s[2]);
            float e_norm = sqrt(e[0] * e[0] + e[1] * e[1] + e[2] * e[2]);
            float ang = acos(dot_product / (e_norm));
            ang = ang * 180.0f / M_PI; // Convert to degrees
            angle.push_back(ang);
        }
    }


    static void calculate_spline_rot() {
        float b_3[4][4] = {
            {-1, 3, -3, 1},
            {3, -6, 3, 0},
            {-3, 0, 3, 0},
            {1, 4, 1, 0}
        };

        int steps = spline_points.size() - 3;
        float t;
        float incr;
        vector<Point> points;
        for (int st = 0; st < steps; ++st) {
            t = 0;
            incr = 0.01;
            copy(spline_points.begin(), spline_points.end(), back_inserter(points));
            float r_mat[4][4] = {
                {points[st].x, points[st].y, points[st].z, 1},
                {points[st + 1].x, points[st + 1].y, points[st + 1].z, 1},
                {points[st + 2].x, points[st + 2].y, points[st + 2].z, 1},
                {points[st + 3].x, points[st + 3].y, points[st + 3].z, 1}
            };

            for (int i = 0; i < 100; ++i) {
                float t_mat[4] = { 6.0f * t, 2.0f, 0.0f, 0.0f };

                float p_t_temp[3] = { 0.0f, 0.0f, 0.0f };
                for (int row = 0; row < 4; ++row) {
                    for (int col = 0; col < 4; ++col) {
                        p_t_temp[0] += t_mat[row] * b_3[row][col] * r_mat[col][0];
                        p_t_temp[1] += t_mat[row] * b_3[row][col] * r_mat[col][1];
                        p_t_temp[2] += t_mat[row] * b_3[row][col] * r_mat[col][2];
                    }
                }

                Point p_t(p_t_temp[0], p_t_temp[1], p_t_temp[2]);
                spline_rot.push_back(p_t);
                t += incr;
            }
        }
    }

};

void draw() {
    
    for (int i = 0; i < calculated_spline_points.size() - 1; i++) {
        glColor3f(1.0, 0, 0.0);
        glBegin(GL_LINES);
        Point p1 = calculated_spline_points[i];
        Point p2 = calculated_spline_points[i + 1];
        glVertex3f(p1.x, p1.y, p1.z);
        glVertex3f(p2.x, p2.y, p2.z);
        glEnd();
    }
    
    Point destination = calculated_spline_points[step];
    Point axis = rot_axis[step];
    glPushMatrix();
    glTranslatef(destination.x, destination.y, destination.z);
    glRotatef(angle[step], axis.x, axis.y, axis.z);
    glScalef(7, 7, 7);
    glColor3f(1.0, 1.0, 0.0);
    for (int i = 0; i < faces.size(); i++) {
        vector<int> face = faces[i];
        Point f1 = verts[face[0] - 1];
        Point f2 = verts[face[1] - 1];
        Point f3 = verts[face[2] - 1];
        glBegin(GL_TRIANGLES);
        glVertex3f(f1.x, f1.y, f1.z);
        glVertex3f(f2.x, f2.y, f2.z);
        glVertex3f(f3.x, f3.y, f3.z);
        glEnd();
    }
    glPopMatrix();
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_LINES);
    Point p1 = calculated_spline_points[step];
    Point p_t = point_tangents[step];
    p_t.normalize_then_scale(4);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(p1.x + p_t.x, p1.y + p_t.y, p1.z + p_t.z);
    glEnd();
}

void display() {
    glClearColor(0, 0, 0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    draw();
    glutSwapBuffers();
}

void reshape(int width, int height) {
    wnd_width = width;
    wnd_height = height;
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, float(width) / height, 0.5, 100);
    gluLookAt(
        eye.x, eye.y, eye.z,
        0.0, 0.0, 30.0,
        0.0, 0.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
}

void updatePerspective() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(45.0, float(wnd_width) / wnd_height, 0.5, 100);
    gluLookAt(
        eye.x, eye.y, eye.z,
        0.0, 0.0, 30.0,
        0.0, 0.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
}

void update(int up) {
    step += 1;
    if (step == max_step) {
        step = 0;
    }
    glutPostRedisplay();
    glutTimerFunc(5, update, 0);
}

void key_pressed(unsigned char key, int x, int y) {
    bool key_pressed = false;
    if (key == 'a') {
        key_pressed = true;
        eye.x = eye.x + 0.2;
    }
    else if (key == 's') {
        key_pressed = true;
        eye.y = eye.y + 0.2;
    }
    else if (key == 'd') {
        key_pressed = true;
        eye.z = eye.z + 0.2;
    }
    else if (key == 'f') {
        key_pressed = true;
        eye.x = eye.x - 0.2;
    }
    else if (key == 'g') {
        key_pressed = true;
        eye.y = eye.y - 0.2;
    }
    else if (key == 'h') {
        key_pressed = true;
        eye.z = eye.z - 0.2;
    }
    else if (key == 'p') {
        cout << "Eye: " << eye << endl;
    }

    if (key_pressed) {
        updatePerspective();
        glutPostRedisplay();
    }
}

class Draw {
public:
    Draw() {
        step = 0;
        max_step = calculated_spline_points.size();
    }

    void start(int argc, char** argv) {
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
        glutInitWindowSize(wnd_width, wnd_height);
        glutInitWindowPosition(20, 20);
        glutCreateWindow(wnd_name.c_str());

        glutDisplayFunc(display);
        glutReshapeFunc(reshape);
        glutKeyboardFunc(key_pressed);

        eye = Point(0, 70, 50);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(80.0, float(wnd_width) / wnd_height, 0.5, 100);
        glMatrixMode(GL_MODELVIEW);

        glutTimerFunc(5, update, 0);
        glutMainLoop();
    }
};

void read_obj(const std::string& file) {
    std::ifstream raw(file);
    std::string line;

    if (!raw.is_open()) {
        std::cerr << "Failed to open the file: " << file << std::endl;
        return;
    }

    while (std::getline(raw, line)) {
        line = line.substr(0, line.find_first_of('#')); // Remove comments

        if (line.empty()) {
            continue; // Skip empty lines
        }

        std::istringstream iss(line);
        char type;
        iss >> type;

        if (type == 'v') {
            float x, y, z;
            if (iss >> x >> y >> z) {
                Point vertex(x, y, z);
                verts.push_back(vertex);
            }
        }
        else if (type == 'f') {
            int v1, v2, v3;
            if (iss >> v1 >> v2 >> v3) {
                faces.push_back({ v1, v2, v3 });
            }
        }
    }
}

int main(int argc, char** argv) {
    Bspline::read_spline_points("spline.txt");
    /*cout << "read spline points";
    for (Point i : spline_points) {
        cout << i;
    }*/
    Bspline::find_spline_points();

    /*cout << "calculated spline points" << endl;
    for (Point i : calculated_spline_points) {
        cout << i;
    }*/
    Bspline::find_spline_tangent();
    Bspline::calculate_model_angle();
    /*
    cout << "model angle" << endl;
    for (float i : angle) {
        cout << i << endl;
    }
    */
    Bspline::calculate_spline_rot();
    /*
    cout << "spline rot" << endl;
    for (Point i : spline_rot) {
        cout << i;
    }
    */

    read_obj("aircraft747.obj");

    Draw renderer;
    renderer.start(argc, argv);

    return 0;
}
