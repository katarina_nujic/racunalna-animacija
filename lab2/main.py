import pyglet
from pyglet.gl import *
from euclid import *
from random import *
import time

window = pyglet.window.Window()

tex = pyglet.image.load('balloon.bmp').get_texture()

class Shape:
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size

    def contains_point(self, x, y):
        return (
            self.x - self.size/2 < x < self.x + self.size/2 and
            self.y - self.size/2 < y < self.y + self.size/2
        )

class BalloonParticle:
    def __init__(self, x, y):
        self.pos = Vector3(x, y, 0)
        self.ds = Vector3(10, 80, 0)
        self.remove_time = time.time() + 100
        self.color = (*self.random_color(), 0.7)  # Random color with alpha component set to 0.7
        self.width = 35     # Set the width of the balloon
        self.height = 70    # Set the height of the balloon

    def random_color(self):
        return (random(), random(), random())

    def update(self, dt):
        self.pos += self.ds * dt

class BalloonParticleSystem:
    def __init__(self, num=1, max_particles=40, shape=None):
        self.particles = []
        self.max_particles = max_particles
        self.shape = shape
        self.time_since_last_particle = 0  # Variable to track the time since the last particle was created

    def add_particles(self, num):
        current_time = time.time()
        for _ in range(num):
            if len(self.particles) <= self.max_particles:
                x = uniform(0, window.width)  # Random x-coordinate within the window width
                y = 0  # Start particles at the bottom
                p = BalloonParticle(x, y)
                self.particles.append(p)
                self.time_since_last_particle = current_time

    def draw(self):
        # Draw the square
        if self.shape != None:
            glColor3f(255, 0, 0)
            glBegin(GL_QUADS)
            glVertex2f(self.shape.x - self.shape.size/2, self.shape.y - self.shape.size/2)
            glVertex2f(self.shape.x + self.shape.size/2, self.shape.y - self.shape.size/2)
            glVertex2f(self.shape.x + self.shape.size/2, self.shape.y + self.shape.size/2)
            glVertex2f(self.shape.x - self.shape.size/2, self.shape.y + self.shape.size/2)
            glEnd()

        # Draw the particles
        glEnable(tex.target)
        glBindTexture(tex.target, tex.id)
        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE)
        glBegin(GL_QUADS)
        for p in self.particles:
            glColor4f(*p.color)  # Set the color of the balloons including alpha
            glTexCoord2f(0, 0)
            glVertex3f(p.pos[0] - p.width, p.pos[1] - p.height, p.pos[2])
            glTexCoord2f(1, 0)
            glVertex3f(p.pos[0] + p.width, p.pos[1] - p.height, p.pos[2])
            glTexCoord2f(1, 1)
            glVertex3f(p.pos[0] + p.width, p.pos[1] + p.height, p.pos[2])
            glTexCoord2f(0, 1)
            glVertex3f(p.pos[0] - p.width, p.pos[1] + p.height, p.pos[2])
        glEnd()
        glDisable(GL_BLEND)
        glDisable(tex.target)

    def update(self, dt):
        for p in self.particles:
            p.update(dt)

        current_time = time.time()
        if current_time - self.time_since_last_particle > 0.3:  # Delay
            self.add_particles(1)

        for i in range(len(self.particles) - 1, -1, -1):
            if (
                self.shape != None and
                (
                    self.shape.contains_point(self.particles[i].pos[0] - self.particles[i].width/2, self.particles[i].pos[1]) or
                    self.shape.contains_point(self.particles[i].pos[0] + self.particles[i].width/2, self.particles[i].pos[1])
                )
                ):
                del self.particles[i]
            elif self.particles[i].pos[1] > window.height + self.particles[i].height:  # Remove particles that go beyond the window
                del self.particles[i]

shape = Shape(window.width // 2, window.height // 2, 120)
systems = [BalloonParticleSystem(max_particles=20, shape=shape)]

@window.event
def on_draw():
    glClearColor(0.0, 0.0, 0.0, 1)
    glClear(GL_COLOR_BUFFER_BIT)
    for s in systems:
        s.draw()

def update(dt):
    for s in systems:
        s.update(dt)
    for i in range(len(systems) - 1, -1, -1):
        if len(systems[i].particles) == 0:
            del systems[i]

pyglet.clock.schedule_interval(update, 1/1000.0)
pyglet.app.run()
