# Računalna animacija
Laboratorijske vježbe iz predmeta Računalna animacija.

Repozitorij sadrži:
* laboratorijsku vježbu 1 (/lab1)
    * **Praćenje putanje**
    * Korištena tehnologija: C++

    <img src="image_lab1.jpg" width="400"/>
* laboratorijsku vježbu 2 (/lab2)
    * **Sustav čestica**
    * Korištena tehnologija: Python

    <img src="image_lab2.png" width="400"/>
* laboratorijsku vježbu 3 (/lab3)
    * Slobodan odabir teme: **Proceduralno generiranje labirinta s algoritmom povratnog praćenja**
    * Korištena tehnologija: Unity

    <img src="image_lab3.png" width="400"/>